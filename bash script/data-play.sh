#!/bin/bash

##############################################################################
# Download the file using wget, rename the file            		    #
#############################################################################

wget -O gutenberg.txt https://www.gutenberg.org/files/2264/2264.txt


##############################################################################
# Show Output of each word count		            		    #
#############################################################################

sed 's/[^a-zA-Z]/ /g' gutenberg.txt | tr \  '\n' | tr '[:upper:]' '[:lower:]' | grep -v "^\s*$" | sort | uniq -c | sort -n


##############################################################################
# Show Output of each word count		            		    #
#############################################################################

sed 's/[^a-zA-Z]/ /g' gutenberg.txt | tr \  '\n' | tr '[:upper:]' '[:lower:]' | grep -v "^\s*$" | sort | uniq -c | sort -nr | head -35>data-report.txt



##############################################################################
# Count total number of words			            		    #
#############################################################################

number_of_words=`wc --word < gutenberg.txt`

echo "Total number of words: $number_of_words"



##############################################################################
# Calculate real percentage			            		    #
#############################################################################

cat data-report.txt | awk -v words=$number_of_words '{ ORS=""};
    {word=$1/words;print $2;
    print " : [";
    for(i=1;i<=word*100;i++) 
            print "="
    print "]",word*100,"%","\n"}'

##############################################################################
# Delete the unrequired files			            		    #
#############################################################################
rm gutenberg.txt data-report.txt
