import urllib.request
import collections
import pandas as pd
import matplotlib.pyplot as plt


# To Download the file from URL

url="https://www.gutenberg.org/files/2264/2264.txt"
urllib.request.urlretrieve(url, "gutenberg.txt")

# Read the file (input)
file = open('gutenberg.txt', encoding="utf8")
a= file.read()

# Words to be neglected

ignore_words = set(['www','com','https'])


# Take the dictonary to be empty at initially, keep adding elements after noting them.
wordcount = {}


# To eliminate duplicates and non-alphabets.
for word in a.lower().split():
    word = word.replace(".","")
    word = word.replace(",","")
    word = word.replace(":","")
    word = word.replace("\"","")
    word = word.replace("!","")
    word = word.replace("â€œ","")
    word = word.replace("â€˜","")
    word = word.replace("*","")
    word = word.replace("(","")
    word = word.replace(")","")
    word = word.replace("?","")
    if word not in ignore_words:
        if word not in wordcount:
            wordcount[word] = 1
        else:
            wordcount[word] += 1



# print all the letters from the file
word_counter = collections.Counter(wordcount)
for word, count in word_counter.most_common():
    print(word, ": ", count)

# Close the file
file.close()

print("Most Common 15 words graph:")

# Print most common word
n_print = 15

# Draw a bar chart
lst = word_counter.most_common(n_print)
df = pd.DataFrame(lst, columns = ['Word', 'Count'])
df.plot.bar(x='Word',y='Count')
plt.show()
